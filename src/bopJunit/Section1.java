package bopJunit;
import static org.junit.Assert.assertEquals;

import org.junit.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
public class Section1 {
	
	WebDriver cd;
	
	@Before
	
	public void beforeTest() {
		System.setProperty("webdriver.chrome.driver", "D:\\ALM image\\Sel\\chromedriver.exe");
		cd = new ChromeDriver();
		cd.get("http://shop.demoqa.com/");
	}
	
	@After
	public void afterTest() {
		cd.close();
	}
	
	@Test
	public void test1() {
		assertEquals("http://shop.demoqa.com/",cd.getCurrentUrl());
	}
	
	@Test
	public void test2() {
		assertEquals("Shoptools",cd.getTitle());
	}
	
	
}
