package bopJunit;

import static org.junit.Assert.assertEquals;

import org.junit.*;
import org.junit.rules.TestName;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Section2 {
	static WebDriver dr;
	@Rule 
	public  TestName testName = new TestName();
	@BeforeClass
	public static void startApp() {
		System.setProperty("webdriver.chrome.driver", "D:\\ALM image\\Sel\\chromedriver.exe");
		dr = new ChromeDriver();
		dr.manage().window().maximize();
		dr.get("http://shop.demoqa.com/");

	}
	
	@AfterClass
	public static void closeApp() {
		dr.close();
	}
	
	@After
	public void aFter() {
		System.out.println("method name is :"+testName.getMethodName());
	}
	
	@Before
	public  void beforeTest() {
		System.setProperty("webdriver.chrome.driver", "D:\\ALM image\\Sel\\chromedriver.exe");
		dr = new ChromeDriver();
		dr.get("http://shop.demoqa.com/");
	}
	
	@Test
	public void test1() {
		assertEquals("http://shop.demoqa.com/",dr.getCurrentUrl());
	}
	
	@Test
	public void test2() {
		assertEquals("Shoptools",dr.getTitle());
	}
}
